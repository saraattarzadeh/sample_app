require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    ActionMailer::Base.deliveries.clear
  end
  
  test "invalid signup information" do
    #not necessary but good for clarity of whats going on and also ensuring the form renders without error
    get signup_path
    
    #check the user count in the db, try to post invalid user, make sure the db count doesn't change (aka no new user is created)
    assert_no_difference 'User.count' do
      post users_path, params: {user: {name: "", email: "user@invalid", password: "foo", password_confirmation: "bar" } }
    end
    
    #this checks that a failed submission re-renders the new action
    assert_template 'users/new'
  end
  
  test "valid signup information with account activation" do
     #not necessary but good for clarity of whats going on and also ensuring the form renders without error
    get signup_path
    
     #check the user count in the db, try to post valid user, make sure the db count changes by 1 (aka 1 new user is created)
    assert_difference 'User.count', 1 do
      post users_path, params: {user: {name: "Example User", email: "user@example.com", password: "password", password_confirmation: "password" } }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    
    # Try to log in before activation.
    log_in_as(user)
    assert_not is_logged_in?
    
    # Invalid activation token
    get edit_account_activation_path("invalid token", email: user.email)
    assert_not is_logged_in?
    
    # Valid token, wrong email
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    
    #valid token, correct email
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    
    #this checks that a successful submission renders the show action
    assert_template 'users/show'
    assert is_logged_in?
  end
end
